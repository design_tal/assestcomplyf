export interface Registration {
    firstname: string;
    lastname: string;
    email: string;
    mobile: string;
    company: string;
    username: string;
    password: string;
    mode: string;
    redirect: string;
  }